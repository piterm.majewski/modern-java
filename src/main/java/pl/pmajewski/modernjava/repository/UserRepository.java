package pl.pmajewski.modernjava.repository;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import pl.pmajewski.modernjava.model.User;

public class UserRepository implements Repository<User> {

	private Set<User> storage = new HashSet<>();
	
	public Stream<User> all() {
		return storage.stream();
	}

	public User add(User item) {
		Long max = all().max(Comparator.comparing(User::getId)).orElse(new User(-1l)).getId();
		item.setId(++max);
		storage.add(item);
		return item;
	}
}
