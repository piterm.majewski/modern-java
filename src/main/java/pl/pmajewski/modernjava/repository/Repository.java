package pl.pmajewski.modernjava.repository;

import java.util.stream.Stream;

public interface Repository <T> {

	Stream<T> all();
	
	T add(T item);
}
