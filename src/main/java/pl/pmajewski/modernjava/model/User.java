package pl.pmajewski.modernjava.model;

import java.time.LocalDate;
import java.util.Set;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class User {
	
	private Long id;
	private String name;
	private Sex sex;
	private LocalDate birthday;
	private Set<Transaction> transactions;
	
	public User(Long id) {
		this.id = id;
	}
	
}
