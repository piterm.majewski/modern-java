package pl.pmajewski.modernjava.model;

public enum Sex {
	MALE, FEMALE
}
