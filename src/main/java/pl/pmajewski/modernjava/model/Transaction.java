package pl.pmajewski.modernjava.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class Transaction {

	public enum Type { BUY }
	
	private Long id;
	private String name;
	private Type type;
	private BigDecimal price;
	private LocalDate date;
	private User user;
	
	public Transaction(Long id) {
		this.id = id;
	}
}
